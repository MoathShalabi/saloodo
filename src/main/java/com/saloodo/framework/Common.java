package com.saloodo.framework;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.saloodo.framework.Base;

public class Common extends Base {

	public static HttpURLConnection huc;
	public static String CurrentNodeID = null;

	// This method responsible for checking the server request code(200)
	public static void getResponseCode(String ProductName, String ProductURL) {
		try {
			URL url = new URL(ProductURL);
			huc = (HttpURLConnection) url.openConnection();
			huc.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
			huc.setRequestMethod("GET");
			huc.connect();
			Assert.assertEquals(huc.getResponseCode(), 200);
			LOGGER.info("Website is OK => " + ProductName);
		} catch (Throwable e) {
			LOGGER.info("( " + ProductName + " ) => " + "WebSite Has Problem Error, URL: " + ProductURL + "\n");
			Assert.fail();
		}
	}

	// @Test
	public static void manageEmails() {

		try {
			// Open Gmail login page
			driver.get("https://mail.google.com/mail/u/0/#label/Saloodo+Reg");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Login to the email address
			driver.findElement(By.xpath(getPageElement("gmail_email"))).sendKeys(getPageElement("shipper_reg_email"));
			driver.findElement(By.xpath(getPageElement("gmail_email_btn"))).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath(getPageElement("gmail_pass"))).sendKeys(getPageElement("shipper_reg_pass"));
			Thread.sleep(10000);
			driver.findElement(By.xpath(getPageElement("gmail_pass_btn"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		/*	boolean staleElement = true;
			while (staleElement) {
				try {
					driver.findElement(By.xpath(getPageElement("gmail_select_all")));
					staleElement = false;

				} catch (StaleElementReferenceException e) {
					staleElement = true;
				}
			}

			// Clear all the emails
			driver.findElement(By.xpath(getPageElement("gmail_select_all"))).click();

			driver.findElement(By.xpath(getPageElement("gmail_delete_btn"))).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			Thread.sleep(10000);

			MailEmptyMsg = driver.findElement(By.xpath(getPageElement("gmail_empty_msg"))).getText();
			MailExpectedMsg = getPageElement("mail_empty_msg").toString();
			Assert.assertEquals(MailExpectedMsg, MailEmptyMsg);
			*/
			
		} catch (Exception e) {
			e.printStackTrace(System.out);
			Assert.fail();
		}
	}

}