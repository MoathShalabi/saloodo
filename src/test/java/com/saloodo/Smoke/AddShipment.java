package com.saloodo.Smoke;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;


import com.saloodo.framework.Common;

/**
 * @Moath Shalabi -+ Create Add New Shipment
 *
 */

public class AddShipment extends Common {
	String cookieValue = "";

	@Test(priority = 1, enabled = true)
	public void addShipment() {
		try {
			
			// Do logn as a shipper
			ShipperLogin.shipperLogin();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.findElement(By.xpath(getPageElement("shipper_skip_alert"))).click();
			
			
			// Click Add Shipment to open the form
			driver.findElement(By.xpath(getPageElement("addshipment_btn"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			// Fill add shipment requirement form
			driver.findElement(By.xpath(getPageElement("trucks_list"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Thread.sleep(5000);
			js.executeAsyncScript("document.getElementById(\"mySelect\").selectedIndex = \"2\";");	
			
			
			driver.findElement(By.id(getPageElement("trucks_number"))).click();
			
			Thread.sleep(10000);
		/*	driver.findElement(By.xpath(getPageElement("trucks_number"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			Thread.sleep(10000);*/

		} catch (Throwable e) {
			e.printStackTrace(System.out);
			Assert.fail();
		}
	}

}
