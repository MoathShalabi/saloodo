package com.saloodo.Smoke;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.saloodo.framework.Common;

/**
 * @Moath Shalabi -+ Create Carrier Registration Test Case
 */

public class CarrierSignup extends Common {

	@Test(priority = 1, enabled = true)
	public void carrierSignup() {
		int RandVat = 0;
		String RandNumb = "";
		String EmailAddress = "";
		String[] parts;
		String ThankUExpected = "";
		String ThankUActual = "";
		try {
			for (int ran = 0; ran < 5; ran++) {
				RandVat = (int) (Math.random() * (10 - 1));
				RandNumb = RandNumb + Integer.toString(RandVat);
			}
			// Click Registration button from the menu
			driver.findElement(By.xpath(getPageElement("reg_btn"))).click();

			// Click Register Your Company button
			driver.findElement(By.xpath(getPageElement("carrier_reg_btn"))).click();

			// Select the country Name United Arab Emirates
			driver.findElement(By.xpath(getPageElement("country_list"))).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath(getPageElement("city_link"))).click();

			// Enter the Company name
			driver.findElement(By.xpath(getPageElement("carrier_company_name")))
					.sendKeys(getPageElement("company_name"));

			// Enter the First name
			driver.findElement(By.xpath(getPageElement("carrier_first_name")))
					.sendKeys(getPageElement("shipper_first_name"));

			// Enter the Last name
			driver.findElement(By.xpath(getPageElement("carrier_last_name")))
					.sendKeys(getPageElement("shipper_last_name"));

			// Enter the email
			EmailAddress = getPageElement("shipper_reg_email").toString();

			parts = EmailAddress.split("@");
			EmailAddress = parts[0] + "+t" + RandNumb + "@" + parts[1];

			driver.findElement(By.xpath(getPageElement("carrier_mail_txt"))).sendKeys(EmailAddress);

			// Enter the Phone number
			driver.findElement(By.xpath(getPageElement("carrier_phone"))).sendKeys(getPageElement("shipper_mob_num"));

			// Enter Password
			driver.findElement(By.xpath(getPageElement("carrier_pass"))).sendKeys(getPageElement("reg_pass"));

			// Click Agree to be contacted by Saloodo
			driver.findElement(By.xpath(getPageElement("agree_contact"))).click();

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Click Agree for TOS
			driver.findElement(By.xpath(getPageElement("agree_tos"))).click();

			Thread.sleep(10000);
			// Click Create Your Saloodo Account
			driver.findElement(By.xpath(getPageElement("create_shipper_btn"))).click();

			Thread.sleep(10000);

			ThankUActual = driver.findElement(By.xpath(getPageElement("carrier_reg_msg"))).getText();
			ThankUExpected = getPageElement("carrier_reg_thanks_msg").toString();

			Assert.assertEquals(ThankUExpected, ThankUActual);

			// Login to the email address
			manageEmails();
			Thread.sleep(10000);

			// Verify Registration Email
			driver.findElement(By.xpath(getPageElement("gmail_open_carriermail"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			System.out.println("moath shalabi");
			Thread.sleep(10000);

			// Get Button link then open it
			driver.findElement(By.xpath(getPageElement("gmail_verify_btn"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			// Enter the new pass
			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			driver.findElement(By.xpath(getPageElement("shipper_reg_setpass"))).sendKeys(getPageElement("reg_pass"));
			driver.findElement(By.xpath(getPageElement("shipper_reg_setpass_btn"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			// Enter the email
			driver.findElement(By.xpath(getPageElement("email_field"))).sendKeys(EmailAddress);

			// Enter the pass
			driver.findElement(By.xpath(getPageElement("pass_field"))).sendKeys(getPageElement("pass"));

			// Click Login button
			driver.findElement(By.xpath(getPageElement("submit_btn"))).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Verify Login with the new Carrier account
			ThankUActual = driver.findElement(By.xpath(getPageElement("carrier_confirm_txt"))).getText();
			ThankUExpected = getPageElement("carrier_reg_confirm_msg").toString();
			Assert.assertEquals(ThankUExpected, ThankUActual);

			// Back to the Gmail tab
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.switchTo().window(tabs2.get(0));

		} catch (Throwable e) {
			e.printStackTrace(System.out);
			Assert.fail();
		}
	}

}
