package com.saloodo.Smoke;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.saloodo.framework.Common;

/**
 * @Moath Shalabi -+ Create Shipper Registration Test Case
 */

public class ShipperSignup extends Common {

	@Test(priority = 1, enabled = true)
	public void shipperSignup() {
		int RandVat = 0;
		String RandNumb = "";
		String EmailAddress = "";
		String[] parts;
		String ThankUExpected = "";
		String ThankUActual = "";
		try {
			for (int ran = 0; ran < 5; ran++) {
				RandVat = (int) (Math.random() * (10 - 1));
				RandNumb = RandNumb + Integer.toString(RandVat);
			}
			// Click Registration button from the menu
			driver.findElement(By.xpath(getPageElement("reg_btn"))).click();

			// Click Register Your Company button
			driver.findElement(By.xpath(getPageElement("shipper_reg_btn"))).click();

			// Enter the company name
			driver.findElement(By.xpath(getPageElement("reg_company"))).sendKeys(getPageElement("reg_company_name"));

			EmailAddress = getPageElement("shipper_reg_email").toString();

			parts = EmailAddress.split("@");
			EmailAddress = parts[0] + "+t" + RandNumb + "@" + parts[1];

			// Enter the email
			driver.findElement(By.xpath(getPageElement("reg_email"))).sendKeys(EmailAddress);

			// Enter the first name
			driver.findElement(By.xpath(getPageElement("first_name"))).sendKeys(getPageElement("shipper_first_name"));

			// Enter the last name
			driver.findElement(By.xpath(getPageElement("last_name"))).sendKeys(getPageElement("shipper_last_name"));

			// Enter the Phone number
			driver.findElement(By.xpath(getPageElement("mobile_num"))).sendKeys(getPageElement("shipper_mob_num"));

			// Enter the VAT ID
			driver.findElement(By.xpath(getPageElement("vat_id"))).sendKeys(getPageElement("shipper_vatID") + RandNumb);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Click Agree to be contacted by Saloodo
			driver.findElement(By.xpath(getPageElement("agree_contact"))).click();

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Click Agree for TOS
			driver.findElement(By.xpath(getPageElement("agree_tos"))).click();

			Thread.sleep(10000);
			// Click Create Your Saloodo Account
			driver.findElement(By.xpath(getPageElement("create_shipper_btn"))).click();

			Thread.sleep(10000);

			ThankUActual = driver.findElement(By.xpath(getPageElement("reg_thanku"))).getText();
			ThankUExpected = getPageElement("shipper_reg_thanks_msg").toString();

			Assert.assertEquals(ThankUExpected, ThankUActual);

			// Login to the email address
			manageEmails();
			Thread.sleep(10000);

			// Verify Registration Email
			driver.findElement(By.xpath(getPageElement("gmail_open_mail"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			System.out.println("moath shalabi");
			Thread.sleep(10000);

			// Get Button link then open it
			driver.findElement(By.xpath(getPageElement("gmail_verify_btn"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			// Enter the new pass
			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			driver.findElement(By.xpath(getPageElement("shipper_reg_setpass"))).sendKeys(getPageElement("reg_pass"));
			driver.findElement(By.xpath(getPageElement("shipper_reg_setpass_btn"))).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			// Enter the email
			driver.findElement(By.xpath(getPageElement("email_field"))).sendKeys(EmailAddress);

			// Enter the pass
			driver.findElement(By.xpath(getPageElement("pass_field"))).sendKeys(getPageElement("pass"));

			// Click Login button
			driver.findElement(By.xpath(getPageElement("submit_btn"))).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			// Back to the Gmail tab
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.switchTo().window(tabs2.get(0));

		} catch (Throwable e) {
			e.printStackTrace(System.out);
			Assert.fail();
		}
	}

}
