package com.saloodo.Smoke;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.saloodo.framework.Common;

/**
 * @Moath Shalabi -+ Create Login as Carrier Test Case
 */

public class CarrierLogin extends Common {
	String cookieValue = "";

	@Test(priority = 1, enabled = true)
	public void carrierLogin() {
		try {

			// Click Login button from the menu
			driver.findElement(By.xpath(getPageElement("login_btn"))).click();

			// Enter the email
			driver.findElement(By.xpath(getPageElement("email_field"))).sendKeys(getPageElement("carrier_email"));

			// Enter the pass
			driver.findElement(By.xpath(getPageElement("pass_field"))).sendKeys("saloodo");
			// Click Login button
			driver.findElement(By.xpath(getPageElement("submit_btn"))).click();

			// Get the cookie name that related to the logged in user and verify its value
			cookieValue = driver.manage().getCookieNamed("hashedUserId").getValue();

			// cookieName= driver.manage().getCookies().toString();
			Assert.assertEquals(cookieValue, getPageElement("carrier_cookie").toString());
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		} catch (Throwable e) {
			e.printStackTrace(System.out);
			Assert.fail();
		}
	}

}
